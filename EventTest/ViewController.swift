//
//  ViewController.swift
//  EventTest
//
//  Created by techfun on 2019/03/08.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var calendarCardView: UIView!
    @IBOutlet weak var weekDayCollectionView: UICollectionView!
    
    @IBOutlet weak var calendarDayCollectionView: UICollectionView!
    
    @IBOutlet weak var yearMonthLabel: UILabel!
    private var Array = [String]()
    private var calendar = Calendar.current
    private var currentDate = Date()
    private var numberOfItems: Int!
    private var daysPerWeek: Int = 7
    private var currntMonthOfDates = [Date]()
    private var selectedDate = Date()
    private var days: Int = 0
    private var currentDay : Int?
    private var currentMonth : Int?
    private var y: Int = 0
    private var m: Int = 0
    private var d: Int = 0
     private var weekDayArr = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    private var eventListDB = EventListDB()
    private var eventList = [EventList]()
    private var selectedEvent = [EventList]()
    private var checkDate : String?
    private let columnLayout = ColumnLayout(
        cellsPerRow: 7
    )
    
    private let weekDayColumnLayout = ColumnLayout(
        cellsPerRow: 7
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        weekDayCollectionView.delegate = self
        weekDayCollectionView.dataSource = self
        
        calendarDayCollectionView.delegate = self
        calendarDayCollectionView.dataSource = self
        
        //Get Current Day, Month, Year
        let date = Date()
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        y = year
        m = month
        d = day
        
        self.calendarDayCollectionView.register(UINib(nibName: "CalendarDayCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "dayCell")
        self.weekDayCollectionView.register(UINib(nibName: "WeekDayCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "weekDayCell")
        firstWeek(year: year, month: month)
        self.navigationItem.title = "Calendar"
        self.setCardView(self.calendarCardView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let eventListFromDB = self.eventListDB.getAllEventList()
        if eventListFromDB.count > 0{
            self.eventList = eventListFromDB
        }
        self.calendarDayCollectionView.reloadData()
    }
    //Set Current Day
    private func setCurrentDay(){
        let todayDate = Date()
        let calendar = Calendar.current
        self.currentDay = calendar.component(.day, from: todayDate)
        self.currentMonth = calendar.component(.month, from: todayDate)
    }
    
    
    // Collection View Function
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.checkDate = "\(y)/\(m)/\(Array[indexPath.row])"
        for (index,value) in self.eventList.enumerated(){
            if self.eventList[index].date == checkDate{
                //eventList.append(self.eventList[index].eventList)
                self.selectedEvent.append(value)
            }
        }
        self.performSegue(withIdentifier: "showEventList", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.weekDayCollectionView{
            return 7
        }else{
            return 42
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.calendarDayCollectionView{
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "dayCell", for: indexPath) as! CalendarDayCollectionViewCell
            
            //Check Event Date and Calendar Date to event title
             let checkDate = "\(y)/\(m)/\(Array[indexPath.row])"
             let selectedEvent = getEventInfoByDay(checkDate)
            
            let screenWidth = UIScreen.main.bounds.size.width
            var eventTitleViewWidth : CGFloat = 0.0
            var eventTitleHeight : Int = 0
            var labelCorner : CGFloat = 0.0
            
            if screenWidth >= 400 {
                eventTitleViewWidth = 53
                eventTitleHeight = 10
                labelCorner = 18.0
            }else if screenWidth < 400 && screenWidth > 370{
                eventTitleViewWidth = 45
                eventTitleHeight = 10
                labelCorner = 18.0
            }else{
                eventTitleViewWidth = 40
                eventTitleHeight = 8
                cell.calendarCellHeight.constant = 25
                cell.calendarCellHeight.constant = 25
                labelCorner = 17.0
            }
            
            
           if selectedEvent.count <= 0{
                
                for subview in cell.eventListView.subviews {
                    if !(subview is UILayoutSupport) {
                        subview.removeFromSuperview()
                    }
                }
                
            }else{
                
                for (index,event) in selectedEvent.enumerated(){
                    var eventTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: Int(eventTitleViewWidth), height: eventTitleHeight))
                    if index == 0 {
                        eventTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: Int(eventTitleViewWidth), height: eventTitleHeight))
                    }else if index == 1 {
                        eventTitleLabel = UILabel(frame: CGRect(x: 0, y: eventTitleHeight + 2, width: Int(eventTitleViewWidth), height: eventTitleHeight))
                    }else if index == 2 {
                        eventTitleLabel = UILabel(frame: CGRect(x: 0, y: eventTitleHeight + 13, width: Int(eventTitleViewWidth), height: eventTitleHeight))
                    }else if index >= 3{
                        break
                    }
                    
                    eventTitleLabel.backgroundColor = UIColor.red
                    eventTitleLabel.layer.masksToBounds = true
                    eventTitleLabel.layer.cornerRadius = 5.0
                    eventTitleLabel.textAlignment = .center
                    eventTitleLabel.textColor = UIColor.white
                    eventTitleLabel.text = event
                    eventTitleLabel.font = UIFont.systemFont(ofSize: 10.0,weight : .bold)
                    
                    cell.eventListView.contentMode = .center
                    cell.eventListView.addSubview(eventTitleLabel)
                }
            }
            
            if ((self.currentDay == Int(Array[indexPath.row])) && (self.currentMonth == self.m)){
                cell.eventDay.textColor = UIColor(red: 0/255, green: 107/255, blue: 204/255,alpha : 1)
                cell.eventDay.backgroundColor = UIColor(red: 215/255, green: 230/255, blue: 255/255,alpha : 1)
                cell.eventDay.layer.cornerRadius = labelCorner
                cell.eventDay.layer.masksToBounds = true
                cell.eventDay.text = Array[indexPath.row]
            }else{
                cell.eventDay.textColor = UIColor.black
                cell.eventDay.backgroundColor = UIColor.clear
                cell.eventDay.text = Array[indexPath.row]
            }
            
            
            
            if (indexPath.row == 0) {
                //check sunday or not
                cell.eventDay.text = Array[indexPath.row]
            }else if (indexPath.row == 6 || indexPath.row == 13 || indexPath.row == 20 || indexPath.row == 27 || indexPath.row == 34 || indexPath.row == 41) {
                //check saturday
                if ((self.currentDay == Int(Array[indexPath.row])) && (self.currentMonth == self.m)){
                    cell.eventDay.textColor = UIColor(red: 0/255, green: 107/255, blue: 204/255,alpha : 1)
                    cell.eventDay.backgroundColor = UIColor(red: 215/255, green: 230/255, blue: 255/255,alpha : 1)
                    cell.eventDay.layer.cornerRadius = labelCorner
                    cell.eventDay.layer.masksToBounds = true
                }else{
                    cell.eventDay.textColor = UIColor.black
                    cell.eventDay.backgroundColor = UIColor.white
                }
                cell.eventDay.text = Array[indexPath.row]
                
            }else if (indexPath.row % 7 == 0) {
                //check sunday by dividing indexpath.row with 7
                if ((self.currentDay == Int(Array[indexPath.row])) && (self.currentMonth == self.m)){
                    cell.eventDay.textColor = UIColor(red: 0/255, green: 107/255, blue: 204/255, alpha : 1)
                    cell.eventDay.backgroundColor = UIColor(red: 215/255, green: 230/255, blue: 255/255,alpha : 1)
                    
                    cell.eventDay.layer.cornerRadius = labelCorner
                    cell.eventDay.layer.masksToBounds = true
                }else{
                    cell.eventDay.textColor = UIColor.black
                    cell.eventDay.backgroundColor = UIColor.white
                }
                cell.eventDay.text = Array[indexPath.row]
                
            }else {
                
            }
            
            if (indexPath.row <= 5) {
                //change color if the day of previous month
                if (cell.eventDay.text == "23" || cell.eventDay.text == "24" || cell.eventDay.text == "25" || cell.eventDay.text == "26" || cell.eventDay.text == "27" || cell.eventDay.text == "28" || cell.eventDay.text == "29" || cell.eventDay.text == "30" || cell.eventDay.text == "31") {
                    cell.eventDay.text = ""
                    cell.eventDay.layer.borderColor = UIColor.clear.cgColor
                    for subview in cell.eventListView.subviews {
                        if !(subview is UILayoutSupport) {
                            subview.removeFromSuperview()
                        }
                    }
                    cell.eventListView.backgroundColor = UIColor.clear
                    cell.eventDay.backgroundColor = UIColor.white
                }
            }else if (indexPath.row >= 28) {
                //change color if the day of next month
                if (cell.eventDay.text == "1" || cell.eventDay.text == "2" || cell.eventDay.text == "3" || cell.eventDay.text == "4" || cell.eventDay.text == "5" || cell.eventDay.text == "6" || cell.eventDay.text == "7" || cell.eventDay.text == "8" || cell.eventDay.text == "9" || cell.eventDay.text == "10" || cell.eventDay.text == "11" || cell.eventDay.text == "12" || cell.eventDay.text == "13" || cell.eventDay.text == "14") {
                    cell.eventDay.text = ""
                    cell.eventDay.layer.borderColor = UIColor.clear.cgColor
                    for subview in cell.eventListView.subviews {
                        if !(subview is UILayoutSupport) {
                            subview.removeFromSuperview()
                        }
                    }
                    cell.eventListView.backgroundColor = UIColor.clear
                    cell.eventDay.backgroundColor = UIColor.white
                }
            }
            
            
            return cell
        }else{
            let weekDayCell = collectionView.dequeueReusableCell(withReuseIdentifier: "weekDayCell", for: indexPath) as! WeekDayCollectionViewCell
            weekDayCell.weekDay.text = self.weekDayArr[indexPath.row]
            return weekDayCell
        }
        
    }
    
    private func getEventInfoByDay(_ checkDate : String) -> [String]{
         var eventTitle : [String] = [String]()
       
            for event in eventList{
                if event.date == checkDate{
                    eventTitle.append(event.eventTitle)
                }
            }
        return eventTitle
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if (UIDevice.modelName == "iPhone SE"){
            return CGSize(width: 40.0, height: 58.0)
        }else if(UIDevice.modelName == "iPhone 8 Plus"){
            return CGSize(width: 53.5, height : 75.0)
        }
        return CGSize(width: 45.0, height: 75.0)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    //Private Function
    func firstWeek(year: Int, month: Int) {
        guard let todayDate = calendar.date(from: DateComponents(year: year, month: month)) else { return }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        dateFormatter.calendar = calendar
        let yearMonthFomatter = DateFormatter()
        yearMonthFomatter.dateFormat = "MMM yyyy"
        yearMonthFomatter.calendar = calendar
        Array.removeAll()
        let todayDateString = yearMonthFomatter.string(from: todayDate)
        self.yearMonthLabel.text = todayDateString
        
        var components = calendar.dateComponents([.year, .month], from: todayDate)
        components.weekOfMonth = 1  //first week
        for weekday in 1...7 {
            components.weekday = weekday  // get week day
            if let date = calendar.date(from: components) {
                Array.append(dateFormatter.string(from: date))
            }
        }
        components.weekOfMonth = 2 //second week
        for weekday in 1...7 {
            components.weekday = weekday  // get week day
            if let date = calendar.date(from: components) {
                Array.append(dateFormatter.string(from: date))
            }
        }
        components.weekOfMonth = 3 //third week
        for weekday in 1...7 {
            components.weekday = weekday  // get week day
            if let date = calendar.date(from: components) {
                Array.append(dateFormatter.string(from: date))
            }
        }
        components.weekOfMonth = 4 //fourth week
        for weekday in 1...7 {
            components.weekday = weekday  // get week day
            if let date = calendar.date(from: components) {
                Array.append(dateFormatter.string(from: date))
            }
        }
        components.weekOfMonth = 5 //fifth week
        for weekday in 1...7 {
            components.weekday = weekday  //get week day
            if let date = calendar.date(from: components) {
                Array.append(dateFormatter.string(from: date))
            }
        }
        components.weekOfMonth = 6 //sixth week
        for weekday in 1...7 {
            components.weekday = weekday  // get week day
            if let date = calendar.date(from: components) {
                Array.append(dateFormatter.string(from: date))
            }
        }
        calendarDayCollectionView.reloadData()
    }
    
    @IBAction func previous(_ sender: Any) {
        if m == 1 {
            y -= 1
            m = 12
        }else {
            m -= 1
        }
        self.setCurrentDay()
        firstWeek(year: y, month: m)
        self.calendarDayCollectionView.reloadData()
    }
    
    @IBAction func next(_ sender: Any) {
        if m == 12 {
            y += 1
            m = 1
        }else {
            m += 1
        }
        self.setCurrentDay()
        firstWeek(year: y, month: m)
        self.calendarDayCollectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventList" {
            if let eventListViewController = segue.destination as? EventListViewController {
                eventListViewController.checkDate = self.checkDate
            }
        }
    }
    
    private func setCardView(_ view : UIView){
        view.layer.cornerRadius = 10.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.7
    }
}


