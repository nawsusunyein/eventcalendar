//
//  EventNewViewController.swift
//  EventTest
//
//  Created by techfun on 2019/03/08.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class EventNewViewController: UIViewController {
    
    
    @IBOutlet var eventDate: UILabel!
    @IBOutlet var eventTitle: SkyFloatingLabelTextField!
    @IBOutlet var eventContent: UITextView!
    @IBOutlet var createEventBtn: UIButton!
    @IBOutlet var eventTimeBtn: UIButton!
    @IBOutlet var eventUpdateBtn: UIButton!
    @IBOutlet var eventDelBtn: UIButton!
    
    
    private let choosenEventTime : String = ""
    private let dateHelper = DateHelper()
    private let eventDB = EventListDB()
    private var timePicker = UIDatePicker()
    private var choosenTime = Date()
    private var eventDateStr : String?
    
    public var isUpdate : Bool!
    public var createDate : String!
    public var delegate : EventListViewController!
    public var updateEvent : EventList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let eventDate = dateHelper.convertStringToDate(self.createDate, "dd MMM yyyy")
        self.eventDateStr = dateHelper.convertDateToString(eventDate, "yyyy/M/dd")
        
        self.eventDate.text = self.createDate
        self.setButtonAttributes(self.eventTimeBtn)
        self.setButtonAttributes(self.createEventBtn)
        self.setButtonAttributes(self.eventUpdateBtn)
        self.setButtonAttributes(self.eventDelBtn)
        self.setDefaultTime()
        self.setDefaultData()
    }
    
    @IBAction func createEvent(_ sender: Any) {
        if self.checkData(){
            let eventTitle = self.eventTitle.text
            let date = self.eventDateStr
            let eventContent = self.eventContent.text
            let eventTime = self.eventTimeBtn.titleLabel?.text
            let currentMili = Date().timeIntervalSince1970
            let event = EventList(date!,eventContent!,eventTitle!,date!,3,Int(currentMili),eventTime!)
            eventDB.insertEventList([event])
            self.showCreateSuccess()
        }else{
            self.showError()
        }
        
    }
    
    @IBAction func showTimePicker(_ sender: Any) {
        let timeAlertController = UIAlertController(title: "", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        self.timePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 280, height: 200))
        self.timePicker.datePickerMode = .time
        self.timePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        self.timePicker.date = self.choosenTime
        self.timePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction) in
            print("ok")
        })
        timeAlertController.view.addSubview(self.timePicker)
        timeAlertController.addAction(okAction)
        self.present(timeAlertController, animated: true, completion: nil)
    }
    
    @objc private func datePickerValueChanged(sender:UIDatePicker) {
        let choosenTime = dateHelper.convertDateToString(sender.date, "HH:mm")
        self.choosenTime = sender.date
        self.eventTimeBtn.setTitle(choosenTime, for: .normal)
    }
    
    private func setButtonAttributes(_ button : UIButton){
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    private func setDefaultData(){
        if self.isUpdate{
            self.eventTitle.text = self.updateEvent.eventTitle
            self.eventDate.text = self.updateEvent.eventDay
            self.eventTimeBtn.setTitle(self.updateEvent.eventTime, for: .normal)
            self.eventContent.text = self.updateEvent.eventList
            self.createEventBtn.isHidden = true
            self.eventUpdateBtn.isHidden = false
            self.eventDelBtn.isHidden = false
        }else{
            self.eventUpdateBtn.isHidden = true
            self.eventDelBtn.isHidden = true
            self.createEventBtn.isHidden = false
        }
    }
    
    private func setDefaultTime(){
        let currentTimeString = dateHelper.convertDateToString(Date(), "HH:mm")
        self.eventTimeBtn.setTitle(currentTimeString, for: .normal)
    }
    
    private func checkData() -> Bool{
        let eventTitle = self.eventTitle.text
        if (eventTitle?.isEmpty)!{
            return false
        }else{
            return true
        }
    }
    
    private func showError(){
        let errorAlert = UIAlertController(title: "Error", message: "Fill title", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction) in
            print("ok action")
        })
        errorAlert.addAction(okAction)
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    private func showCreateSuccess(){
        let successAlert = UIAlertController(title: "", message: "Created Successfully", preferredStyle: .alert)
        let _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(dismissAlert), userInfo: nil, repeats: false)
        
        self.present(successAlert, animated: true, completion: nil)
    }
    
    @objc private func dismissAlert(){
        self.dismiss(animated: true, completion: nil)
        self.delegate.sendEventNewCreatedDate(self.eventDateStr!)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateEvent(_ sender: Any) {
        let eventContent = self.eventContent.text
        let eventTime = self.eventTimeBtn.titleLabel?.text
        let eventTitle = self.eventTitle.text
        self.eventDB.updateEvent(self.updateEvent.eventID,eventContent!,eventTitle!,eventTime!)
        self.showSuccessEventAlert("Update successfully")
    }
    
    private func showSuccessEventAlert(_ message : String){
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        let _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(dismissForAlertView), userInfo: nil, repeats: false)
    }
    
    @objc private func dismissForAlertView(){
        self.dismiss(animated: true, completion: nil)
        self.delegate.sendEventNewCreatedDate(self.eventDateStr!)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteEvent(_ sender: Any) {
        self.eventDB.deleteEventByDate(self.updateEvent.eventID)
        self.showSuccessEventAlert("Delete successfully")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol EventNewDelegate {
    func sendEventNewCreatedDate(_ createdDate : String)
    
}
