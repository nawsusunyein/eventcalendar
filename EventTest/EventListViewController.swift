//
//  EventListViewController.swift
//  EventTest
//
//  Created by techfun on 2019/03/08.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class EventListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var eventListTable: UITableView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var createEventBtn: UIButton!
    
    public var selectedEvent : [EventList]!
    public var checkDate : String?
    
    private var updateEvent : EventList!
    private var dateHelper = DateHelper()
    private var eventListDB = EventListDB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventListTable.register(UINib(nibName: "EventListTableViewCell", bundle: nil), forCellReuseIdentifier: "eventListTableCell")
        self.setEventDate()
        self.selectedEvent = self.eventListDB.getEventListByDate(self.checkDate!)
        self.setButtonAttributes()
        self.backBarButton()
        if self.selectedEvent.count > 0 {
            self.rightBarButton()
        }
    }
    
    func backBarButton(){
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 209/255, green: 112/255, blue: 57/255, alpha: 1)
    }
    
    func rightBarButton(){
        let delImg = UIImage(named: "delete")?.withRenderingMode(.alwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: delImg, style: .plain, target: self, action: #selector(deleteAllEvent))
    }
    
    @objc func deleteAllEvent(){
        self.showConfirmDeleteAllEvent()
        
    }
    
    func showConfirmDeleteAllEvent(){
        let alertController = UIAlertController(title: "", message: "Are you sure to delete?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction) in
            self.deleteEventByDay()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(UIAlertAction) in
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteEventByDay(){
        self.eventListDB.deleteEventByDateString(self.checkDate!)
        self.showDeleteSuccessAlert()
        
    }
    
    func showDeleteSuccessAlert(){
        let alertController = UIAlertController(title: "", message: "Deleted Successfully", preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        let _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(dismissAlert), userInfo: nil, repeats: false)
        
    }
    
    @objc func dismissAlert(){
        self.dismiss(animated: true, completion: nil)
        self.selectedEvent.removeAll()
        self.eventListTable.reloadData()
    }
    
    
    func setEventDate(){
        let eventDate = dateHelper.convertStringToDate(self.checkDate!, "yyyy/M/dd")
        let eventDateStr = dateHelper.convertDateToString(eventDate, "dd MMM yyyy")
        self.dateLabel.text = eventDateStr
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectedEvent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventListTableCell", for: indexPath) as! EventListTableViewCell
        let event = self.selectedEvent[indexPath.row]
        cell.eventTitle.text = event.eventTitle
        cell.eventTime.text = event.eventTime
        cell.eventContent.text = event.eventList
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.updateEvent = self.selectedEvent[indexPath.row]
        self.performSegue(withIdentifier: "showEventCreate", sender: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func setButtonAttributes(){
        self.createEventBtn.layer.cornerRadius = 5.0
    }
    
    @IBAction func goToEventCreate(_ sender: Any) {
        self.performSegue(withIdentifier: "showEventCreate", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        switch segue.identifier {
        case "showEventCreate":
            if let eventNewViewController = segue.destination as? EventNewViewController{
                    eventNewViewController.createDate = self.dateLabel.text
                    eventNewViewController.delegate = self
                if self.updateEvent == nil{
                    eventNewViewController.isUpdate = false
                }else{
                    eventNewViewController.isUpdate = true
                    eventNewViewController.updateEvent = self.updateEvent
                }
            }
            break
        default:
            break
        }
    }
    

}

extension EventListViewController : EventNewDelegate{
    func sendEventNewCreatedDate(_ createdDate: String) {
        let eventDateStr = createdDate
        self.selectedEvent = self.eventListDB.getEventListByDate(eventDateStr)
        self.eventListTable.reloadData()
        
    }
    
    
}
