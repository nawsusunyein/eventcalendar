//
//  CalendarDayCollectionViewCell.swift
//  EventTest
//
//  Created by techfun on 2019/03/08.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class CalendarDayCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var eventListView: UIView!
    
    @IBOutlet weak var calendarCellHeight: NSLayoutConstraint!
    @IBOutlet weak var eventDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

