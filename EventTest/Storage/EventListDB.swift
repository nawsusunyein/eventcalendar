//
//  EventListDB.swift
//  EventTest
//
//  Created by techfun on 2019/03/12.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import Foundation
import SQLite3

class EventListDB{
    private let EVENT_LIST_TABLE = "EventList"
    private let EVENT_DATE = "date"
    private let EVENT_CONTENT = "eventList"
    private let EVENT_TITLE = "eventTitle"
    private let EVENT_DAY = "eventDay"
    private let EVENT_MONTH = "eventMonth"
    private let EVENT_ID = "eventID"
    private let EVENT_TIME = "eventTime"
    
    private var db: OpaquePointer?
    
    init(){
        //the database file
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("SBApp.sqlite")
        
        //opening the database
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("Error opening database")
        }
        
        //creating a statement
        var stmt: OpaquePointer?
        
        let queryString = "CREATE TABLE IF NOT EXISTS \(EVENT_LIST_TABLE)" +
            " (\(EVENT_DATE) TEXT," +
            " \(EVENT_CONTENT) TEXT," +
            "\(EVENT_TITLE) TEXT," +
            "\(EVENT_DAY) TEXT," +
            "\(EVENT_MONTH) INTEGER," +
            "\(EVENT_ID) INTERGER," +
        "\(EVENT_TIME) TEXT)"
        
        //creating table
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) == SQLITE_OK {
            if sqlite3_step(stmt) == SQLITE_DONE {
                print("Event List Table Created")
            } else {
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("Event list talbe can't be created : \(errmsg)")
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Create table statement can't be prepared : \(errmsg)")
        }
        sqlite3_finalize(stmt)
    }
    
    
    func insertEventList(_ eventList : [EventList]){
        var stmt: OpaquePointer?
        
        for event in eventList{
            let queryString = "INSERT INTO \(self.EVENT_LIST_TABLE)" +
                " (\(EVENT_DATE)," +
                " \(EVENT_CONTENT)," +
                " \(EVENT_TITLE)," +
                " \(EVENT_DAY)," +
                " \(EVENT_MONTH)," +
                "\(EVENT_ID)," +
            "\(EVENT_TIME)) VALUES (?,?,?,?,?,?,?);"
            
            //preparing the query
            if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("\(self.EVENT_LIST_TABLE) : Error preparing insert: \(errmsg)")
                sqlite3_finalize(stmt)
                return
            }else{
                let eventDate = event.date as NSString
                let eventContent = event.eventList as NSString
                let eventTitle = event.eventTitle as NSString
                let eventDay = event.eventDay as NSString
                let eventMonth = Int32(event.eventMonth)
                let eventId = Int32(event.eventID)
                let eventTime = event.eventTime as NSString
                
                //binding the parameters
                if sqlite3_bind_text(stmt, 1, eventDate.utf8String ,-1,nil) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding  : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_bind_text(stmt, 2, eventContent.utf8String ,-1,nil) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding  : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_bind_text(stmt, 3, eventTitle.utf8String ,-1,nil) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding  : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_bind_text(stmt, 4, eventDay.utf8String ,-1,nil) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding  : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_bind_int(stmt, 5, eventMonth) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding  : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_bind_int(stmt, 6, eventId) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_bind_text(stmt, 7, eventTime.utf8String,-1, nil) != SQLITE_OK{
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Failure binding  : \(errmsg)")
                    sqlite3_finalize(stmt)
                    return
                }
                
                if sqlite3_step(stmt) == SQLITE_DONE {
                    print("\(self.EVENT_LIST_TABLE) : Successfully inserted row. \(eventDay)")
                } else {
                    let errmsg = String(cString: sqlite3_errmsg(db)!)
                    print("\(self.EVENT_LIST_TABLE) : Could not insert row. \(errmsg)")
                }
                
                sqlite3_finalize(stmt)
            }
            
        }
        
    }
    
    func getAllEventList() -> [EventList]{
        var stmt:OpaquePointer?
        var eventList = [EventList]()
        
        let queryString = "SELECT * FROM \(self.EVENT_LIST_TABLE);"
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Error preparing query all: \(errmsg)")
            sqlite3_finalize(stmt)
            return eventList
        }
        
        var list : [EventList]?
        
        while(sqlite3_step(stmt) == SQLITE_ROW){
            if(list == nil){
                list = [EventList]()
            }
            
            let eventDate = String(cString:sqlite3_column_text(stmt,0))
            let eventContent = String(cString:sqlite3_column_text(stmt,1))
            let eventTitle = String(cString:sqlite3_column_text(stmt,2))
            let eventDay = String(cString:sqlite3_column_text(stmt, 3))
            let eventMonth = sqlite3_column_int(stmt, 4)
            let eventId = sqlite3_column_int(stmt, 5)
            let eventTime = String(cString:sqlite3_column_text(stmt, 6))
            let event = EventList.init(eventDate, eventContent, eventTitle, eventDay, Int(eventMonth),Int(eventId),eventTime)
            
            list?.append(event)
        }
        if list != nil && (list?.count)! > 0{
            eventList = list!
        }
        
        sqlite3_finalize(stmt)
        return eventList
    }
    
    func getEventListByDate(_ eventDate : String) -> [EventList]{
        var stmt : OpaquePointer?
        var eventList = [EventList]()
        
        let eventDateString = "'" + eventDate + "'"
        let queryString = "SELECT * FROM \(self.EVENT_LIST_TABLE) WHERE \(self.EVENT_DATE) = \(eventDateString);"
        
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Error preparing query all : \(errmsg)")
            sqlite3_finalize(stmt)
            return eventList
        }
        
        var list : [EventList]?
        
        while(sqlite3_step(stmt) == SQLITE_ROW){
            if(list == nil){
                list = [EventList]()
            }
            
            let eventDate = String(cString:sqlite3_column_text(stmt, 0))
            let eventContent = String(cString: sqlite3_column_text(stmt, 1))
            let eventTitle = String(cString: sqlite3_column_text(stmt, 2))
            let eventDay = String(cString: sqlite3_column_text(stmt, 3))
            let eventMonth = sqlite3_column_int(stmt, 4)
            let eventId = sqlite3_column_int(stmt, 5)
            let eventTime = String(cString:sqlite3_column_text(stmt, 6))
            let event = EventList.init(eventDate, eventContent, eventTitle, eventDay, Int(eventMonth),Int(eventId),eventTime)
            list?.append(event)
        }
        
        if list != nil && (list?.count)! > 0{
            eventList = list!
        }
        sqlite3_finalize(stmt)
        return eventList
    }
    
    
    func deleteAllEventList(){
        
        var stmt:OpaquePointer?
        let queryString = "DELETE FROM \(self.EVENT_LIST_TABLE) ;"
        
        
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Error preparing to delete: \(errmsg)")
        }
        
        if sqlite3_step(stmt) == SQLITE_DONE {
            print("\(self.EVENT_LIST_TABLE) : Successfully deleted row.")
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Could not delete row: \(errmsg)")
        }
        sqlite3_finalize(stmt)
    }
    
    func deleteEventByDate(_ eventID : Int){
        var stmt : OpaquePointer?
        let queryString = "DELETE FROM \(self.EVENT_LIST_TABLE) WHERE \(self.EVENT_ID) = \(eventID);"
        
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Error preparing to delete : \(errmsg)")
        }
        
        if sqlite3_step(stmt) == SQLITE_DONE{
            print("\(self.EVENT_LIST_TABLE) : Successfully deleted row . \(eventID)")
        }else{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Could not delte row : \(errmsg)")
        }
        sqlite3_finalize(stmt)
    }
    
    func deleteEventByDateString(_ eventDate : String){
        var stmt : OpaquePointer?
        let eventDateStr = "'" + eventDate + "'"
        let queryString = "DELETE FROM \(self.EVENT_LIST_TABLE) WHERE \(self.EVENT_DATE) = \(eventDateStr);"
        
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Error preparing to delete : \(errmsg)")
        }
        
        if sqlite3_step(stmt) == SQLITE_DONE{
            print("\(self.EVENT_LIST_TABLE) : Successfully deleted row . \(eventDate)")
        }else{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Could not delte row : \(errmsg)")
        }
        sqlite3_finalize(stmt)
    }
    
    func updateEvent(_ eventId : Int,_ eventContent : String,_ eventTitle : String,_ eventTime : String){
        
        let content = "'" + eventContent + "'"
        let title = "'" + eventTitle + "'"
        let eventTime = "'" + eventTime + "'"
        
        let updateCommentStatementString = "UPDATE \(self.EVENT_LIST_TABLE) SET \(self.EVENT_CONTENT) = \(content),\(self.EVENT_TITLE) = \(title),\(self.EVENT_TIME) = \(eventTime) WHERE \(self.EVENT_ID) = \(eventId);"
        
        var stmt:OpaquePointer?
        if sqlite3_prepare_v2(db, updateCommentStatementString, -1, &stmt, nil) == SQLITE_OK {
            if sqlite3_step(stmt) == SQLITE_DONE {
                print("\(self.EVENT_LIST_TABLE) :  Successfully updated row for event \(eventId)")
            }else{
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("\(self.EVENT_LIST_TABLE) :  Could not update row: \(errmsg)")
            }
        }else{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("\(self.EVENT_LIST_TABLE) : Error preparing update: \(errmsg)")
            
        }
        sqlite3_finalize(stmt)
        
    }
    
}
