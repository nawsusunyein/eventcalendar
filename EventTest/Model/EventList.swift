//
//  EventList.swift
//  EventTest
//
//  Created by techfun on 2019/03/12.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import Foundation

class EventList{
    var date : String
    var eventList : String
    var eventTitle : String
    var eventDay : String
    var eventMonth : Int
    var eventID : Int
    var eventTime : String
    
    init(_ date : String , _ eventList : String, _ eventTitle : String,_ eventDay : String,_ eventMonth : Int,_ eventID : Int,_ eventTime : String){
        self.date = date
        self.eventList = eventList
        self.eventTitle = eventTitle
        self.eventDay = eventDay
        self.eventMonth = eventMonth
        self.eventID = eventID
        self.eventTime = eventTime
    }
}

