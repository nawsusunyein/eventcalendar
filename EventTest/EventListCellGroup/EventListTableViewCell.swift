//
//  EventListTableViewCell.swift
//  EventTest
//
//  Created by techfun on 2019/03/08.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell {

    @IBOutlet var eventTitle: UILabel!
    @IBOutlet var eventTime: UILabel!
    @IBOutlet var eventContent: UILabel!
    @IBOutlet var eventCardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cardView(self.eventCardView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cardView(_ view : UIView){
        view.layer.cornerRadius = 10.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 5.0
        view.layer.shadowOpacity = 0.7
    }
}
