//
//  DateHelper.swift
//  EventTest
//
//  Created by techfun on 2019/03/13.
//  Copyright © 2019 Naw Su Su Nyein. All rights reserved.
//

import Foundation
class DateHelper{
    
    public func convertDateToString(_ date : Date , _ format : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    public func convertStringToDate(_ stringDate : String,_ format:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: stringDate)
        return date!
    }
}
